<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', 'AdminController@index')->name('adminHome');
// Route::get('/logout', 'AdminController@logout')->name('adminLogout');
// 
// Route::get('/search', 'UserController@search');
// Route::get('/barang/{id}', 'UserController@single');

// Auth::routes(['verify' => false]);
Auth::routes();

Route::group(['admin','middleware' => ['web', 'auth']], function (){

        
        Route::get('/', function(){
            return view ('welcome');
        }); 
        
        Route::get('/', function(){
        if (Auth::user()->admin==1) {
            $user = Auth::user();
                return view('templateAdmin.page.home', [
                "user" => $user
                ]);
        }if (Auth::user()->admin==2) {
            $user = Auth::user();
                return view('templateSekolah.page.home', [
                "user" => $user
                ]);
        } else {
            $user = Auth::user();
            return view('templateUser.page.home', [
            "user" => $user
            ]);
        }
        });
});

// admin route
Route::prefix('admin')->group(function() {

    //user
    Route::get('/home', 'UserController@index')->name('index');
    Route::get('/tugas', 'UserController@tugas')->name('userTugas');
    Route::get('/editProfile', 'UserController@edit_profile')->name('userEditProfile');

    //sekolah
    Route::get('/home', 'SekolahController@indexSekolah')->name('sekolahHome');
    Route::get('/registrasiUser', 'SekolahController@userRegistrasi')->name('userRegistrasi');
    Route::get('/dataPeserta', 'SekolahController@data_peserta')->name('dataPeserta');
    Route::get('/kehadiran', 'SekolahController@kehadiran_peserta')->name('kehadiran');
    Route::get('/NilaiAkhir', 'SekolahController@nilai_akhir_peserta')->name('nilaiAkhirPeserta');
    Route::get('/EditProfile', 'SekolahController@edit_profile_sekolah')->name('sekolahEditProfile');

    //perusahaan
    Route::get('/home', 'AdminController@indexAdmin')->name('adminHome');
    Route::get('/DataTugas', 'AdminController@dataTugas')->name('dataTugas');
    Route::get('/DataPeserta', 'AdminController@dataPeserta')->name('dataPesertaAdmin');
    Route::get('/DetailPeserta', 'AdminController@detailPeserta')->name('detailPeserta');

    Route::get('/logout', 'AdminController@logout')->name('adminLogout');
    
    
    // Route::get('/pengiriman', 'AdminController@pengiriman')->name('adminPengiriman');
    // Route::get('/kategori', 'AdminController@kategori_barang')->name('adminKategoriBarang');

    // Route::post('/pengiriman', 'AdminController@tambah_jasa_pengiriman_controller')->name('adminTambahJasaPengirimanController');
    // Route::get('/barang', 'AdminController@barang')->name('adminBarang');
    // Route::get('/barang/tambahBarang', 'AdminController@tambah_barang')->name('adminTambahBarang');
    // Route::post('/barang', 'AdminController@tambah_barang_controller')->name('adminTambahBarangController');
    // Route::get('/barang/{id}/detail', 'AdminController@detail_barang_controller')->name('adminDetailBarangController');
    // Route::get('/barang/{id}/edit', 'AdminController@edit_barang')->name('adminEditBarang');
    // Route::patch('/barang/{id}', 'AdminController@edit_barang_controller')->name('adminEditBarangController');
    // Route::delete('/barang/{id}', 'AdminController@hapus_barang_controller')->name('adminHapusBarangController');
    // Route::delete('/pengiriman/{id}', 'AdminController@hapus_jasa_pengiriman_controller')->name('adminHapusJasaPengirimanController');
    // Route::post('/pengiriman/{id}/edit', 'AdminController@edit_jasa_pengiriman_controller')->name('adminEditJasaPengirimanController');
    // Route::post('/kategori', 'AdminController@tambah_kategori_barang')->name('adminTambahKategoriBarang');
    // Route::delete('/kategori/{id}', 'AdminController@hapus_kategori_barang')->name('adminHapusKategoriBarang');
    // Route::post('/kategori/{id}/edit', 'AdminController@edit_kategori_barang')->name('adminEditKategoriBarang');
});


Route::get('/home', 'HomeController@index')->name('home');