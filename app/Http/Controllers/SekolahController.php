<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use File;

class SekolahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     //sekolah
    
    public function indexSekolah()
    {
        $user = Auth::user();
        return view('templateSekolah.page.home', [
            "user" => $user
        ]);
    }
    public function data_peserta() {
        $user = Auth::user();
        return view('templateSekolah.page.dataPeserta',[
            "user" => $user
        ]);
    }
    public function kehadiran_peserta() {
        $user = Auth::user();
        return view('templateSekolah.page.kehadiran',[
            "user" => $user
        ]);
    }
    public function nilai_akhir_peserta() {
        $user = Auth::user();
        return view('templateSekolah.page.nilaiAkhir',[
            "user" => $user
        ]);
    }
    public function edit_profile_sekolah() {
        $user = Auth::user();
        return view('templateSekolah.page.editProfile',[
            "user" => $user
        ]);
    }
    public function userRegistrasi() {
        $user = Auth::user();
        return view('templateSekolah.page.userRegistrasi',[
            "user" => $user
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
 
}
