<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\DataTugas;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use File;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $datatugas = DB::table('t_tugas')->get();
        $user = Auth::user();
            return view('templateUser.page.home', [
            'datatugas' => $datatugas,
            "user" => $user
            ]);
    }
    public function tugas() 
    {
        $datatugas = DB::table('t_tugas')->get();

        $user = Auth::user();
        return view('templateUser.page.tableTugas', [
            "datatugas" => $datatugas,
            "user" => $user
        ]);
    }
    public function edit_profile() {
        $user = Auth::user();
        return view('templateUser.page.editProfile',[
            "user" => $user
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
 
}
