<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\MetodePembayaran;
use App\JasaPengiriman;
use App\DetailMetodePembayaran;
use App\KategoriBarang;
use App\Barang;
use App\FotoBarang;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use File;
use App\Http\Controllers\Helper\Image;
use App\Http\Controllers\Helper\FotoBarangHelper;
use App\Http\Controllers\Helper\Pembayaran;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function indexAdmin()
    {
        $user = Auth::user();
        return view('templateAdmin.page.home', [
            "user" => $user
        ]);
    }
    public function dataPeserta()
    {
        $user = Auth::user();
        return view('templateAdmin.page.dataPeserta', [
            "user" => $user
        ]);
    }
    public function dataTugas()
    {
        $user = Auth::user();
        return view('templateAdmin.page.dataTugas', [
            "user" => $user
        ]);
    }
    public function detailPeserta()
    {
        $user = Auth::user();
        return view('templateAdmin.page.detailPeserta', [
            "user" => $user
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }


    // public function pengiriman()
    // {
    //     $jasapengiriman = JasaPengiriman::all();
    //     $user = Auth::user();
    //     return view('templateUser.page.jasaPengiriman', [
    //         "jasapengiriman" => $jasapengiriman,
    //         "user" => $user
    //     ]);
    // }

    // public function tambah_jasa_pengiriman_controller(Request $request)
    // {
    //     $penjual = Auth::user()->id;
    //     $request->validate([
    //         'nama' => 'string|max:50',
    //         'lama_pengiriman' => 'integer',
    //         'harga_per_kilo' => 'integer'
    //     ]);

    //     $input = $request->all();

    //     $status = JasaPengiriman::create($input);

    //     return ($status) ? redirect(route('adminPengiriman')) : die($status);
    // }

    // public function tambah_barang()
    // {
    //     $kategori = KategoriBarang::all();
    //     $user = Auth::user();
    //     return view('templateUser.page.tambahBarang', [
    //         "kategori" => $kategori,
    //         "user" => $user
    //     ]);
    // }

    // public function generate_id_barang() {
    //     $asd = "";
    //     do {
    //         $randomInt = rand(1, 25);
    //         $randomString = str::random($randomInt);
    //         $prefix = "INV";
    //         $asd = $prefix . $randomString;
    //         $id = Barang::where('id_barang', $asd)->get();
    //     } while (!$id->isEmpty());
    //     return $asd;
    // }

    // public function tambah_barang_controller(Request $request)
    // {
    //     // id barang
    //     $idBarang = $this->generate_id_barang();

    //     // penjual
    //     $penjual = Auth::user()->id;

    //     // validasi
    //     $request->validate([
    //         'nama_barang' => 'string|max:200',
    //         'harga_satuan' => 'integer',
    //         'berat' => 'integer|max:10',
    //         'stock' => 'integer',
    //         'pembelian_minimum' => 'required|min:1',
    //         'foto.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
    //     ]);

        // request semua data
    //     $input = $request->all();

    //     // set id penjual
    //     $input['id_penjual'] = $penjual;
    //     $input['id_barang'] = $idBarang;

    //     // get foto[] dari request
    //     $foto['foto'] = $request->file('foto');

    //     // hapus request foto
    //     unset($input['foto']);

    //     // create barang sesuai request
    //     $status = Barang::create($input);

    //     // create foto barang
    //     for($i = 0; $i < count($foto['foto']); $i++) {
    //         $fotoBarang = new FotoBarangHelper($foto['foto'][$i], $idBarang);
    //         $fotoBarang->store();
    //     }

    //     // redirect ketika success
    //     return ($status) ? 
    //         redirect('/admin/barang') :
    //         redirect('/admin/barang/tambahBarang');
    // }

    // public function detail_barang_controller($id)
    // {
    //     $barang = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')->find($id);
    //     $fotoBarang = FotoBarang::where('id_barang', $id)->get();
    //     $user = Auth::user();
    //     return view('templateUser.page.detailBarang', [
    //         "barang" => $barang,
    //         "fotoBarang" => $fotoBarang,
    //         "user" => $user
    //     ]);
    // }

    // public function edit_barang($id)
    // {
    //     $barang = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')
    //                         ->find($id);
    //     $kategori = KategoriBarang::all();
    //     $user = Auth::user();
    //     $fotoBarang = FotoBarang::where('id_barang', $id)->get();
    //     return view('templateUser.page.tambahBarang', [
    //         "barang" => $barang,
    //         "kategori" => $kategori,
    //         "user" => $user,
    //         "fotoBarang" => $fotoBarang
    //     ]);
    // }

    // public function edit_barang_controller(Request $request, $id)
    // {
    //     $penjual = Auth::user()->id;
    //     $request->validate([
    //         'nama_barang' => 'string|max:200',
    //         'harga_satuan' => 'integer',
    //         'berat' => 'integer|max:10',
    //         'stock' => 'integer',
    //         'pembelian_minimum' => 'required|min:1',
    //         'foto.*' => 'mimes:jpeg,png,jpg,gif|max:2048'
    //     ]);

    //     $input = $request->all();

    //     if($request->hasfile('foto')) {
    //         $foto['foto'] = $request->file('foto');
    //         $fotoBarangHelper = new FotoBarangHelper($foto['foto'], $id);
    //         $fotoBarangHelper->update();
    //     }

    //     unset($input['foto']);

    //     $input['id_penjual'] = $penjual;

    //     $status = Barang::find($id)->update($input);

    //     return ($status) ? 
    //         redirect('/admin/barang') :
    //         redirect('/admin/barang/tambahBarang');
    // }

    // public function hapus_barang_controller($id)
    // {
    //     // 1. Hapus foto dari public folder
    //     File::deleteDirectory(public_path() . "/foto_barang/$id");

    //     // 2. Hapus foto
    //     FotoBarang::where('id_barang', $id)->delete();

    //     // 3. Hapus Barang
    //     $status = Barang::find($id)->delete();

    //     $user = Auth::user();

    //     $barang = Barang::where('id_penjual', $user->id)
    //                     ->join('kategori_barang', 'barang.kategori_barang', '=', 'kategori_barang.id_kategori')
    //                     ->get();

    //     return view('templateUser.page.tableBarang', [
    //         "barang" => $barang,
    //         "user" => $user
    //     ]);
    // }


    // public function tambah_kategori_barang(Request $request) {

    //     // 1. Validasi input
    //     $request->validate([
    //         'nama_kategori' => 'required|string|max:50'
    //     ]);

    //     // 2. Ambil data semua Request
    //     $input = $request->all();

    //     // 3. Tambahkan data ke database
    //     $status = KategoriBarang::create($input);

    //     // 4. Redirect page
    //     return ($status) ? 
    //         redirect('/admin/kategori') : die($status);
    // }

    // public function hapus_kategori_barang($id) {
    //     $status = KategoriBarang::find($id)->delete();
    //     return redirect('/admin/kategori');
    // }

    // public function edit_kategori_barang(Request $request, $id) {
    //     $input = $request->all();
    //     KategoriBarang::find($id)->update($input);
    //     return redirect('/admin/kategori');
    // }

    
}
