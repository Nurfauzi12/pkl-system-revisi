@include('templateUser.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Home</a>
      </li>
    </ul>
    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
          <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('./') }}" class="brand-link">
      <img src="{{ asset('images/logo/tab-icon2.png') }}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('images/profile_user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('./') }}" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-header">Home</li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-header">Tugas</li>
          <li class="nav-item">
            <a href="{{ route('userTugas') }}" class="nav-link">
              <i class="fas fa-tasks"></i>
              <p class="text">Tugas</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('userEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
          <li class="nav-item" style="margin-top: 150px; background-color: red; border-radius: 5px" >
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
              </ol>
              </div><!-- /.col -->
              </div><!-- /.row -->
              </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-info elevation-1"><i class="far fa-hand-paper"></i></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Hadir</span>
                        <span class="info-box-number">100
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-walking"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Izin</span>
                        <span class="info-box-number">0</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <!-- fix for small devices only -->
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3" >
                      <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-procedures" style="color: white"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Sakit</span>
                        <span class="info-box-number">0</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-exclamation"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Tanpa Keterangan</span>
                        <span class="info-box-number">0</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- Info boxes -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">
                        Status
                        </h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body" style="display: flex;">
                        <div class="form-check" style="margin-right: 10px;">
                          <input class="form-check-input" type="radio" name="radio1">
                          <label class="form-check-label">Hadir</label>
                        </div>
                        <div class="form-check" style="margin-right: 10px;">
                          <input class="form-check-input" type="radio" name="radio1">
                          <label class="form-check-label">Sakit</label>
                        </div>
                        <div class="form-check" style="margin-right: 10px;">
                          <input class="form-check-input" type="radio" name="radio1">
                          <label class="form-check-label">Izin</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="radio1">
                          <label class="form-check-label">Tanpa Keterangan</label>
                        </div>
                      </div>
                      <div class="card-body" style="margin-top: -20px">
                        <div class="form-group">
                          <label>Keterangan</label>
                          
                          <textarea class="form-control" rows="5" placeholder="Enter ..." style="margin-bottom: 10px"></textarea>
                          <button type="button" class="btn btn-block btn-primary">Kirim</button>
                        </div>
                      </div>
                    </div>
                    <!-- /.card-body -->
                    
                    
                    <!-- /.card -->
                  </div>
                  <!-- /.col (left) -->
                  <div class="col-md-6">
                    <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Tugas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 315px;">
                <table class="table table-head-fixed">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Task</th>
                      <th>Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    )
                    <?php $i = 1; ?>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Backend</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-success">Approved</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Alexander Pierce</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-warning">Pending</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Bob Doe</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-primary">Approved</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Mike Doe</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-danger">Denied</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Jim Doe</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-success">Approved</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Victoria Doe</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-warning">Pending</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Michael Doe</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-primary">Approved</span></td>
                    </tr>
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>Rocky Doe</td>
                      <td>11-7-2014</td>
                      <td><span class="tag tag-danger">Denied</span></td>
                    </tr>
                    <?php $i++; ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
                  </div>
                  <!-- /.col (right) -->
                </div>
                <!-- /.row -->
                <div class="card card-default">
                  <div class="card-header">
                    <h3 class="card-title">Dialy Report</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body" style="display: block;">
                    <div class="row">
                      <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                      <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Submit</button>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                </div>
                </div><!--/. container-fluid -->

              </section>
              <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
              <!-- To the right -->
              <div class="float-right d-sm-none d-md-block">
                Mabar skuii
              </div>
              <!-- Default to the left -->
              <strong>Copyright &copy; 2019 <a href="https://www.arkamaya.co.id/">Arkamaya</a>.</strong> All rights reserved.
            </footer>
          </div>
          @include('templateUser.footer')