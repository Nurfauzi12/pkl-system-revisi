@include('templateUser.header')

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('./') }}" class="brand-link">
      <img src="{{ asset('./images/logo/tab-icon2.png') }}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('images/profile_user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('./') }}" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Home</li>
          <li class="nav-item">
            <a href="{{ url('./') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-header">Tugas</li>
              <li class="nav-item">
                  <a href="{{ route('userTugas') }}" class="nav-link">
                    <i class="fas fa-tasks"></i>
                      <p class="text">Tugas</p>
                  </a>
              </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
          <li class="nav-item" style="margin-top: 150px; background-color: red; border-radius: 5px" >
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" style="margin-top: -10px;">
      <div class="container-fluid">
        <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="card-body">
                       <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter name">
                       </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                       </div>
                       <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter username">
                       </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">No Telepon</label>
                        <input type="number" class="form-control" id="" placeholder="Enter No Telp">
                      </div>
                        <button class="btn btn-primary" type="submit"><i class="far fa-save"></i></i> &nbsp; Simpan</button>
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="fotoBarang">Foto Anda</label>
                        <input type="file" multiple class="form-control" id="fotoBarang" placeholder="Nama Barang" name="foto[]" value="{{ old('foto[]', @$barang->foto) }}" onchange="preview()">
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        Foto Anda
                      </div>
                      <div class="card-body" id="imageUploadSection">
                        {{-- image --}} 
                        @if (!empty($fotoBarang))
                            @foreach ($fotoBarang as $item)
                                <img src="{{ asset("foto_barang/$item->id_barang/$item->foto") }}" class="img-thumbnail col-4 fotoBarang" alt="">
                            @endforeach
                        @endif
                      </div>
                    </div>
                  </div>
              </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Mabar skuii
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="https://www.arkamaya.co.id/">Arkamaya</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->


@include('templateUser.footer')

<script>
  $('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var kategori = button.data('kategori') // Extract info from data-* attributes
    var link = button.data('link')
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    console.log(kategori)
    modal.find('.modal-title').text('Edit Kategori ' + kategori)
    modal.find('#form').attr('action', link)
    modal.find('.modal-body input').val(kategori)
  })
</script>
