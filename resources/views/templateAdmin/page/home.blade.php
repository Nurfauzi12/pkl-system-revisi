@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
            <a href="{{ route('dataPesertaAdmin') }}" class="nav-link">
              <img src="https://img.icons8.com/dotty/80/000000/conference-call.png/20/000000">
              <p style="color: black;">Data peserta</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('dataTugas') }}" class="nav-link">
              <img src="https://img.icons8.com/dotty/80/000000/task-completed.png/20/000000">
              <p style="color: black;">Tugas</p>
            </a>
          </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Penilaian</p>
            </a>
          </li>
          <li class="nav-item" style="background-color: red; text-align: center; margin-top: 430px; position: fixed;width: 18.5%;display: block;">
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>20</h3>

                <p>Jumlah Peserta</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>4</h3>

                <p>Jumlah Sekolah</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="card bg-gradient-success">
              <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">

                <h3 class="card-title">
                  <i class="far fa-calendar-alt"></i>
                  Calendar
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                  <!-- button with a dropdown -->
                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-bars"></i></button>
                    <div class="dropdown-menu float-right" role="menu">
                      <a href="#" class="dropdown-item">Add new event</a>
                      <a href="#" class="dropdown-item">Clear events</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /. tools -->
              </div>
<!-- /.card-header -->
<div class="card-body pt-0">
    <!--The calendar -->
    <div id="calendar" style="width: 100%">
        <div class="bootstrap-datetimepicker-widget usetwentyfour">
            <ul class="list-unstyled">
                <li class="show">
                    <div class="datepicker">
                        <div class="datepicker-days" style="">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Month"></span></th>
                                        <th class="picker-switch" data-action="pickerSwitch" colspan="5" title="Select Month">October 2019</th>
                                        <th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Month"></span></th>
                                    </tr>
                                    <tr>
                                        <th class="dow">Su</th>
                                        <th class="dow">Mo</th>
                                        <th class="dow">Tu</th>
                                        <th class="dow">We</th>
                                        <th class="dow">Th</th>
                                        <th class="dow">Fr</th>
                                        <th class="dow">Sa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-action="selectDay" data-day="09/29/2019" class="day old weekend">29</td>
                                        <td data-action="selectDay" data-day="09/30/2019" class="day old">30</td>
                                        <td data-action="selectDay" data-day="10/01/2019" class="day">1</td>
                                        <td data-action="selectDay" data-day="10/02/2019" class="day">2</td>
                                        <td data-action="selectDay" data-day="10/03/2019" class="day">3</td>
                                        <td data-action="selectDay" data-day="10/04/2019" class="day">4</td>
                                        <td data-action="selectDay" data-day="10/05/2019" class="day weekend">5</td>
                                    </tr>
                                    <tr>
                                        <td data-action="selectDay" data-day="10/06/2019" class="day weekend">6</td>
                                        <td data-action="selectDay" data-day="10/07/2019" class="day">7</td>
                                        <td data-action="selectDay" data-day="10/08/2019" class="day">8</td>
                                        <td data-action="selectDay" data-day="10/09/2019" class="day">9</td>
                                        <td data-action="selectDay" data-day="10/10/2019" class="day">10</td>
                                        <td data-action="selectDay" data-day="10/11/2019" class="day active today">11</td>
                                        <td data-action="selectDay" data-day="10/12/2019" class="day weekend">12</td>
                                    </tr>
                                    <tr>
                                        <td data-action="selectDay" data-day="10/13/2019" class="day weekend">13</td>
                                        <td data-action="selectDay" data-day="10/14/2019" class="day">14</td>
                                        <td data-action="selectDay" data-day="10/15/2019" class="day">15</td>
                                        <td data-action="selectDay" data-day="10/16/2019" class="day">16</td>
                                        <td data-action="selectDay" data-day="10/17/2019" class="day">17</td>
                                        <td data-action="selectDay" data-day="10/18/2019" class="day">18</td>
                                        <td data-action="selectDay" data-day="10/19/2019" class="day weekend">19</td>
                                    </tr>
                                    <tr>
                                        <td data-action="selectDay" data-day="10/20/2019" class="day weekend">20</td>
                                        <td data-action="selectDay" data-day="10/21/2019" class="day">21</td>
                                        <td data-action="selectDay" data-day="10/22/2019" class="day">22</td>
                                        <td data-action="selectDay" data-day="10/23/2019" class="day">23</td>
                                        <td data-action="selectDay" data-day="10/24/2019" class="day">24</td>
                                        <td data-action="selectDay" data-day="10/25/2019" class="day">25</td>
                                        <td data-action="selectDay" data-day="10/26/2019" class="day weekend">26</td>
                                    </tr>
                                    <tr>
                                        <td data-action="selectDay" data-day="10/27/2019" class="day weekend">27</td>
                                        <td data-action="selectDay" data-day="10/28/2019" class="day">28</td>
                                        <td data-action="selectDay" data-day="10/29/2019" class="day">29</td>
                                        <td data-action="selectDay" data-day="10/30/2019" class="day">30</td>
                                        <td data-action="selectDay" data-day="10/31/2019" class="day">31</td>
                                        <td data-action="selectDay" data-day="11/01/2019" class="day new">1</td>
                                        <td data-action="selectDay" data-day="11/02/2019" class="day new weekend">2</td>
                                    </tr>
                                    <tr>
                                        <td data-action="selectDay" data-day="11/03/2019" class="day new weekend">3</td>
                                        <td data-action="selectDay" data-day="11/04/2019" class="day new">4</td>
                                        <td data-action="selectDay" data-day="11/05/2019" class="day new">5</td>
                                        <td data-action="selectDay" data-day="11/06/2019" class="day new">6</td>
                                        <td data-action="selectDay" data-day="11/07/2019" class="day new">7</td>
                                        <td data-action="selectDay" data-day="11/08/2019" class="day new">8</td>
                                        <td data-action="selectDay" data-day="11/09/2019" class="day new weekend">9</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="datepicker-months" style="display: none;">
                            <table class="table-condensed">
                                <thead>
                                    <tr>
                                        <th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Year"></span></th>
                                        <th class="picker-switch" data-action="pickerSwitch" colspan="5" title="Select Year">2019</th>
                                        <th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Year"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7"><span data-action="selectMonth" class="month">Jan</span><span data-action="selectMonth" class="month">Feb</span><span data-action="selectMonth" class="month">Mar</span><span data-action="selectMonth" class="month">Apr</span><span data-action="selectMonth" class="month">May</span><span data-action="selectMonth" class="month">Jun</span><span data-action="selectMonth" class="month">Jul</span><span data-action="selectMonth" class="month">Aug</span><span data-action="selectMonth" class="month">Sep</span><span data-action="selectMonth" class="month active">Oct</span><span data-action="selectMonth" class="month">Nov</span><span data-action="selectMonth" class="month">Dec</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="datepicker-years" style="display: none;">
                            <table class="table-condensed">
                                <thead>
                                    <tr>
                                        <th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Decade"></span></th>
                                        <th class="picker-switch" data-action="pickerSwitch" colspan="5" title="Select Decade">2010-2019</th>
                                        <th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Decade"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7"><span data-action="selectYear" class="year old">2009</span><span data-action="selectYear" class="year">2010</span><span data-action="selectYear" class="year">2011</span><span data-action="selectYear" class="year">2012</span><span data-action="selectYear" class="year">2013</span><span data-action="selectYear" class="year">2014</span><span data-action="selectYear" class="year">2015</span><span data-action="selectYear" class="year">2016</span><span data-action="selectYear" class="year">2017</span><span data-action="selectYear" class="year">2018</span><span data-action="selectYear" class="year active">2019</span><span data-action="selectYear" class="year old">2020</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="datepicker-decades" style="display: none;">
                            <table class="table-condensed">
                                <thead>
                                    <tr>
                                        <th class="prev" data-action="previous"><span class="fa fa-chevron-left" title="Previous Century"></span></th>
                                        <th class="picker-switch" data-action="pickerSwitch" colspan="5">2000-2090</th>
                                        <th class="next" data-action="next"><span class="fa fa-chevron-right" title="Next Century"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7"><span data-action="selectDecade" class="decade old" data-selection="2006">1990</span><span data-action="selectDecade" class="decade" data-selection="2006">2000</span><span data-action="selectDecade" class="decade active" data-selection="2016">2010</span><span data-action="selectDecade" class="decade" data-selection="2026">2020</span><span data-action="selectDecade" class="decade" data-selection="2036">2030</span><span data-action="selectDecade" class="decade" data-selection="2046">2040</span><span data-action="selectDecade" class="decade" data-selection="2056">2050</span><span data-action="selectDecade" class="decade" data-selection="2066">2060</span><span data-action="selectDecade" class="decade" data-selection="2076">2070</span><span data-action="selectDecade" class="decade" data-selection="2086">2080</span><span data-action="selectDecade" class="decade" data-selection="2096">2090</span><span data-action="selectDecade" class="decade old" data-selection="2106">2100</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </li>
                <li class="picker-switch accordion-toggle"></li>
            </ul>
        </div>
    </div>
</div>
              <!-- /.card-body -->
            </div>
      </div>

          </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Mabar skuii
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="https://www.arkamaya.co.id/">Arkamaya</a>.</strong> All rights reserved.
  </footer>
</div>
@include('templateAdmin.footer')
