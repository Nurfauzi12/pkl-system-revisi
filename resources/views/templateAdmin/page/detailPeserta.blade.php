@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
            <a href="{{ route('dataPesertaAdmin') }}" class="nav-link active">
              <img src="https://img.icons8.com/dotty/80/000000/conference-call.png/20/000000">
              <p style="color: black;">Data peserta</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('dataTugas') }}" class="nav-link">
              <img src="https://img.icons8.com/dotty/80/000000/task-completed.png/20/000000">
              <p style="color: black;">Tugas</p>
            </a>
          </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Penilaian</p>
            </a>
          </li>
          <li class="nav-item" style="background-color: red; text-align: center; margin-top: 430px; position: fixed;width: 18.5%;display: block;">
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Peserta</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('adminHome')}}">Home</a></li>
              <li class="breadcrumb-item active">peserta</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row" style="margin-bottom: 10px;">
          <div class="col-3">
          <button type="button" class="btn btn-block btn-primary" style="">
            <a href="{{ route('dataPeserta')}}" style="color: white">
            <i class="fas fa-arrow-left" style="margin-right: 10px;color: white;"></i>Kembali</button>
            </a>
          </div>
            <div class="col-3" style="margin-left: 540px">
          <button type="button" class="btn btn-block btn-primary" style="">
            <i class="far fa-save" style="margin-right: 10px;"></i>Save peserta</button>
          </div>
          </div>
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-3">
                  <h5>NISN</h5>
                    <input type="text" class="form-control" placeholder="" disabled>
                </div>
                <div style="margin-left: 5px;">
                  <h5>Kreativitas</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 50%">
                </div>
                <div class="col-3">
                  <h5>Ketelitian</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 25%">
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-3">
                <h5>Username</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                  <div class="col-3">
                  </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-3">
                <h5>Password</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                <div style="margin-left: 5px;">
                  <h5>Sistematika</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 50%">
                </div>
                <div class="col-3">
                  <h5>Inisiatif</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 25%">
                </div>
                  <div class="col-3">
                    <label for="fotoBarang">Foto Anda</label>
                            <input type="file" multiple class="form-control" id="fotoBarang" placeholder="Nama Barang" name="" value="">  
                  </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-3">
                <h5>Nama peserta</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                  <div class="col-3">
                <h5>Sekolah</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                <div style="margin-left: 5px;">
                  <h5>Tanggung jawab</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 50%">
                </div>
                <div class="col-3">
                  <h5>Komunikasi</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 25%">
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-3">
                <h5>No. telepon</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-3">
                <h5>Tanggal masuk</h5>
                    <input type="text" class="form-control" placeholder="" disabled>
                  </div>
                <div style="margin-left: 5px;">
                  <h5>Penyesuaian diri</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 50%">
                </div>
                <div class="col-3">
                  <h5>Kerja sama</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 25%">
                </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-3">
                <h5>Tanggal keluar</h5>
                    <input type="text" class="form-control" placeholder="" disabled>
                  </div>
              </div>
              <div class="row" style="margin-top: 5px;">
                <div class="col-3">
                <h5>Email</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                <div style="margin-left: 5px;">
                  <h5>Disiplin</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 50%">
                </div>
                <div class="col-3">
                  <h5>Kehadiran</h5>
                    <input type="text" class="form-control" placeholder="" style="width: 25%">
                </div>
              </div>                        
          </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
@include('templateAdmin.footer')
