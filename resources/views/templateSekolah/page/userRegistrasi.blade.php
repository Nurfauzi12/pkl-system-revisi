@include('templateSekolah.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('./') }}" class="brand-link">
      <img src="{{ asset('images/logo/tab-icon2.png') }}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('images/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('./') }}" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Home</li>
          <li class="nav-item">
            <a href="{{ url('./') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-header">Peserta</li>
              <li class="nav-item">
                  <a href="{{ route('dataPeserta') }}" class="nav-link">
                    <i class="fas fa-user"></i>
                      <p class="text">Peserta</p>
                  </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kehadiran') }}" class="nav-link">
                  <i class="fas fa-user"></i>
                  <p class="text">Kehadiran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('nilaiAkhirPeserta') }}" class="nav-link">
                  <i class="fas fa-user"></i>
                  <p class="text">Nilai Akhir Peserta</p>
                </a>
              </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('userRegistrasi') }}" class="nav-link active">
              <i class="fas fa-user-plus"></i>
              <p class="text">Registrasi Peserta</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolahEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
          <li class="nav-item" style="margin-top: 75px; background-color: red; border-radius: 5px" >
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Registrasi Peserta</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Account</a></li>
              <li class="breadcrumb-item active">Registrasi Peserta</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-body" style="display: flex;">
              <form action="" method="" style="width: 45%">
                <div class="form-group">
                  <label for="jumlahPeserta">Jumlah Peserta</label>
                  <select name="" id="jumlahPeserta" class="form-control">
                    <option value="pilih">Pilih</option>
                    <option value="pilih">1 Peserta</option>
                    <option value="pilih">2 Peserta</option>
                    <option value="pilih">3 Peserta</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" class="form-control" id="username" placeholder="Masukkan usrname">
                </div>
                <div class="form-group">
                  <label for="namaPeserta">Nama Peserta</label>
                  <input type="text" class="form-control" id="namaPeserta" placeholder="Masukkan Nama Peserta">
                </div>
                <div class="form-group">
                  <label for="nis">NIS</label>
                  <input type="text" class="form-control" id="nis" placeholder="Masukkan NIS">
                </div>
                <div class="form-group">
                  <label for="email">E-Mail</label>
                  <input type="text" class="form-control" id="email" placeholder="Masukkan E-Mail">
                </div>
                <div class="form-group">
                  <label for="tel">No. Telepon</label>
                  <input type="text" class="form-control" id="tel" placeholder="Masukkan No. Telepon">
                </div>
                <div class="form-group">
                  <label for="pw">Password</label>
                  <input type="Password" class="form-control" id="pw" placeholder="Masukkan Password">
                </div>
                <div class="inout">
                  <div class="form-group" style="width: 48%; float: left; margin-right: 4%;">
                    <label for="in">Tanggal Masuk</label>
                    <input type="text" class="form-control" id="in" placeholder="Masukkan Tanggal Masuk">
                  </div>
                  <div class="form-group" style="width: 48%; float: left;">
                    <label for="out">Tanggal Keluar</label>
                    <input type="text" class="form-control" id="out" placeholder="Masukkan Tanggal Keluar">
                  </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
@include('templateSekolah.footer')

