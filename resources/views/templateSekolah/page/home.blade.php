@include('templateSekolah.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('images/logo/tab-icon2.png') }}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('images/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Home</li>
          <li class="nav-item">
            <a href="{{ url('./') }}" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-header">Peserta</li>
              <li class="nav-item">
                  <a href="{{ route('dataPeserta') }}" class="nav-link">
                    <i class="fas fa-user"></i>
                      <p class="text">Peserta</p>
                  </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kehadiran') }}" class="nav-link">
                  <i class="fas fa-user"></i>
                  <p class="text">Kehadiran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('nilaiAkhirPeserta') }}" class="nav-link">
                  <i class="fas fa-user"></i>
                  <p class="text">Nilai Akhir Peserta</p>
                </a>
              </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('userRegistrasi') }}" class="nav-link">
              <i class="fas fa-user-plus"></i>
              <p class="text">Registrasi Peserta</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolahEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
          <li class="nav-item" style="margin-top: 75px; background-color: red; border-radius: 5px" >
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard Sekolah</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <!-- content -->
            <div class="info-box" style="background-color: gray">
              <span class="info-box-icon info-box-number" style="background-color: lightgray "></span>

              <div class="info-box-content" >
                <span class="info-box-text" style="color: white">Kapasitas Peserta Prakerin</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>

        <div class="col-md-6">
            <div class="card" style="background-color:  lightgray">
             <img src="{{ asset('images/logo/arkamaya.png') }}" height="100px" width="100px">
             <h1 align="center" style="float: right;margin-top: -80px; font-size: 60px">ARKAMAYA</h1>
              <!-- /.card-body -->
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="card"style="background-color:  orange">
              <div class="card-header">
                <h1 class="card-title">
                   About Us
                </h1>
              </div>
              <!-- /.card-header -->
               <div style="padding: 5px">
                 <p>
                    The word "Arkamaya" comes from the Sanskrit language and means "Enlightenment from Heaven". Starting a business is not an easy thing to do but with enlightenment we found the courage to begin. We hope our employees will develop the same passion so we can grow together into a well-established company.      
                 </p>
               </div>
              <!-- /.card-body -->
            </div>
          </div>

          <div class="col-md-6">
            <div class="card"style="background-color:  black">
              <div class="card-header" >

                <h1 class="card-title" style="color: white">
                   Who We Are?
                </h1>
              </div>
              <!-- /.card-header -->
               <div style="padding: 5px">
                 <p style="color: white">
                    First incorporated as CV. Arkamaya Teknologi Digital in early 2011, within two years, we created another larger business and simplified our name to PT. ARKAMAYA in the hope of becoming more accepted in middle to top level industry.          
                 </p>
                 <br>
               </div>
              <!-- /.card-body -->
            </div>
          </div>

             <div class="col-md-6">
            <div class="card"style="background-color:  black">
              <div class="card-header">
                <h1 class="card-title" style="color: white">
                 Our Vision & Mission
                </h1>
              </div>
              <!-- /.card-header -->
               <div style="padding: 5px">
                 <p style="color: white">
                  (V) Our vision to be The most valuable IT Company for supporting human resources and IT Solutions

                  (M) To provide high quality and innovative IT solutions in a timely manner to a diverse range of clients.
                 </p>
               </div>        
              <!-- /.card-body -->
            </div>
          </div>

             <div class="col-md-6">
            <div class="card"style="background-color:  orange">
              <div class="card-header">
                <h1 class="card-title">
                 Corporate Values
                </h1>
              </div>
              <!-- /.card-header -->
               <div>
                 <p style="padding: 5px">
                   We work hard to build relationships based on TRUST and DISCIPLINE as it's a key to providing human resources and IT solutions. And We strive for CONTINOUS IMPROVEMENT in the quality of our work.
                 </p>
               </div>
              <!-- /.card-body -->
            </div>
           </div>
            
          <div class="card"style="background-color:  lightgray">
              <div class="card-header">
                <h1 class="card-title">
                 Little History
                </h1>
              </div>
              <!-- /.card-header -->
              <div class="card-body"style="background-color:  lightgray">
                <h4>
                  Learning By Doing (2011 - 2013)
                </h4>
                <p style="padding-left: 10px">
                  Arkamaya started with a project for PT. TMMIN. We also provided Application Maintenance Support for PD. Kebersihan Bandung. Officially incorporated as CV. Arkamaya Teknologi Digital and opened up our office on Tamansari Street.
                </p>
                <h4>
                  Knowing More (2013 - 2015)
                </h4>
                <p style="padding-left: 10px">
                  We moved into a larger office in Batununggal, Buah Batu so that we could serve more people. We learned many things as an IT company. We continue our support in software development area for PT. TMMIN in its Karawang plant. We also created a new Main Company business, PT. Arkamaya.
                </p>
                <h4>
                  Continous Improvement (2015 - Now)
                </h4>
                <p style="padding-left: 10px">
                  We began to work on several projects in the PT. TMMIN Head Office in Sunter, including PT. Toyota Astra Motor and PT. PUPUK KUJANG in Cikampek. This year of growth encouraged us to begin our Marketing & Sales division to re-define our product we developed to offer to other business partners and clients.
                </p>
              </div>

               <div class="card"style="background-color:  black">
              <div class="card-header">
                <h3 class="card-title">
                  <b style="color: white">
                   Information Box
                  </b>
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body"style="background-color:  black">
                <h5 style="color: white">
                  Peserta
                </h5>
                <p style="padding-left: 10px; color: white">
                 In the sidebar you can find peserta, in peserta you can see the biotada, end score, and absent from your student.  
                </p>
                <h5 style="color: white">
                  Registrasi
                </h5>
                <p style="padding-left: 10px; color: white">
                 In the registrasi you can register your student to be participant PKL in this Industry.
                </p>
                <h5 style="color: white">
                  Edit Profile
                </h5>
                <p style="padding-left: 10px; color: white">
                 In edit profile you can edit your profile if there was something wrong in your profile.  
                </p>
              </div>
                  <!-- /.card-body -->
            </div>
        <!-- /.row -->
         </section>
      </div><!--/. container-fluid -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
@include('templateSekolah.footer')
